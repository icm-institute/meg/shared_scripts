#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 12:46:19 2021

@author: christophe.gitton
"""

############# functions to compute mean head positions, plot head positions, compute tSSS with head position transposition to mean head position #############

import mne
from mne.transforms import (apply_trans, invert_transform, Transform)
from mne.viz._3d import _fiducial_coords
from mayavi import mlab # works only with MNE/0.16.2_py36 for check_head_positions(files_to_process, mean_hp_fname)
import numpy as np
import os.path as op


##### compute mean head position accross several files ############
def mean_hp_compute(files_to_process, mean_hp_fname):
    if not op.exists(mean_hp_fname):
        pos=np.zeros((len(files_to_process), 4, 4))
        i=0
        for fname in files_to_process:
            raw = mne.io.read_raw_fif(fname, allow_maxshield=True)
            pos[i] = raw.info["dev_head_t"]["trans"]
            i=i+1
        ch_names = raw.info["ch_names"]
        sfreq = raw.info["sfreq"]
        mean_hp_info = mne.create_info(ch_names, sfreq=sfreq)
        mean_hp_info["dev_head_t"]["trans"] = np.mean(pos, axis=0)
        mean_hp_info['dig'] = raw.info['dig']
        data = np.ones([len(ch_names), 1])
        mean_hp_raw = mne.io.RawArray(data, mean_hp_info)    
        mean_hp_raw.save(mean_hp_fname, overwrite=True)
    return mean_hp_fname

##### visualize original head positions and the mean head position #########
# works only with MNE/0.16.2_py36 
def check_head_positions(files_to_process, mean_hp_fname):
    #### plot mean head position
    raw = mne.io.read_raw_fif(mean_hp_fname, allow_maxshield=True, verbose=True)
    head_trans = invert_transform(raw.info['dev_head_t'])
    meg_trans = Transform('meg', 'meg')
    car_loc = _fiducial_coords(raw.info['dig'])
    car_loc[:] = apply_trans(invert_transform(raw.info['dev_head_t']), car_loc)
    data = car_loc
    alpha = 1
    scale = 0.003
    fig = mlab.figure(bgcolor=(0.5, 0.5, 0.5), size=(800, 800))
    # draw MEG helmet axes
    axes = [(meg_trans, (0., 0.6, 0.6))]
    for ax in axes:
        x, y, z = np.tile(ax[0]['trans'][:3, 3], 3).reshape((3, 3)).T
        u, v, w = ax[0]['trans'][:3, :3]
        mlab.points3d(x[0], y[0], z[0], color=ax[1], scale_factor=3e-3)
        mlab.quiver3d(x, y, z, u, v, w, mode='arrow', scale_factor=2e-2,
                          color=ax[1], scale_mode='scalar', resolution=20,
                          scalars=[0.33, 0.66, 1.0])
    # draw fiducial points
    points = mlab.points3d(data[0, 0], data[0, 1], data[0, 2],
                                       color=(1.0, 0.0, 0.0), mode='cube', scale_factor=scale,
                                       opacity=alpha, figure=fig)
    points.actor.property.backface_culling = True
    points = mlab.points3d(data[1, 0], data[1, 1], data[1, 2],
                                       color=(0.0, 0.0, 1.0), mode='cube', scale_factor=scale,
                                       opacity=alpha, figure=fig)
    points.actor.property.backface_culling = True
    points = mlab.points3d(data[2, 0], data[2, 1], data[2, 2],
                                       color=(0.0, 1.0, 0.0), mode='cube', scale_factor=scale,
                                       opacity=alpha, figure=fig)
    points.actor.property.backface_culling = True
    # draw Left and right labels
    L = mlab.text3d(data[0, 0]-0.012, data[0, 1], data[0, 2], 'LEFT', color=(0,0,0), scale=0.002, opacity=1)
    R = mlab.text3d(data[2, 0]+0.003, data[0, 1], data[2, 2], 'RIGHT', color=(0,0,0), scale=0.002, opacity=1)
    # draw lines
    x_mid_point = [(data[0, 0]+data[2, 0])/2]
    y_mid_point = [(data[0, 1]+data[2, 1])/2]
    z_mid_point = [(data[0, 2]+data[2, 2])/2]
    mid_point  = np.array([x_mid_point, y_mid_point, z_mid_point], dtype='float32').T
    data = np.vstack((data, mid_point))
    color = (0,0,0)
    mlab.plot3d([data[0, 0], data[2, 0]], [data[0, 1], data[2, 1]], [data[0, 2], data[2, 2]], tube_radius=0.0001, tube_sides=20, color=color)
    mlab.plot3d([data[1, 0], data[3, 0]], [data[1, 1], data[3, 1]], [data[1, 2], data[3, 2]], tube_radius=0.0001, tube_sides=20, color=color)
    #### plot raw files head positions
    color_incr = round(1/(len(files_to_process)+1), 2)
    i=1.0-color_incr
    j=1.0-color_incr
    k=1.0-color_incr
    lines_color = (0,0,0)
    alpha = 0.5
    scale = 0.003
    for fname in files_to_process:
        raw = mne.io.read_raw_fif(fname, allow_maxshield=True, verbose=True)
        head_trans = invert_transform(raw.info['dev_head_t'])
        meg_trans = Transform('meg', 'meg')
        car_loc = _fiducial_coords(raw.info['dig'])
        car_loc[:] = apply_trans(invert_transform(raw.info['dev_head_t']), car_loc) 
        data = car_loc
        ## draw fiducial points
        points = mlab.points3d(data[0, 0], data[0, 1], data[0, 2],
                                       color=(i, 0.0, 0.0), scale_factor=scale,
                                       opacity=alpha, figure=fig)
        points.actor.property.backface_culling = True  
        points = mlab.points3d(data[1, 0], data[1, 1], data[1, 2],
                                       color=(0.0, 0.0, j), scale_factor=scale,
                                       opacity=alpha, figure=fig)
        points.actor.property.backface_culling = True
        points = mlab.points3d(data[2, 0], data[2, 1], data[2, 2],
                                       color=(0.0, k, 0.0), scale_factor=scale,
                                       opacity=alpha, figure=fig)
        points.actor.property.backface_culling = True
        # draw lines
        x_mid_point = [(data[0, 0]+data[2, 0])/2]
        y_mid_point = [(data[0, 1]+data[2, 1])/2]
        z_mid_point = [(data[0, 2]+data[2, 2])/2]
        mid_point  = np.array([x_mid_point, y_mid_point, z_mid_point], dtype='float32').T
        data = np.vstack((data, mid_point))
        mlab.plot3d([data[0, 0], data[2, 0]], [data[0, 1], data[2, 1]], [data[0, 2], data[2, 2]], tube_radius=0.0001, tube_sides=20, color=lines_color)
        mlab.plot3d([data[1, 0], data[3, 0]], [data[1, 1], data[3, 1]], [data[1, 2], data[3, 2]], tube_radius=0.0001, tube_sides=20, color=lines_color)        
        i=i-color_incr
        j=j-color_incr
        k=k-color_incr
    return fig

##### compute tsss with head position transformation to mean head position #######
##### for instance, bad_ch = ['MEG1032', 'MEG2313']
def trans_tsss_compute(input_fname, mean_hp_fname, output_fname, sss_cal, ct_sparse, bad_ch=None):
    raw = mne.io.read_raw_fif(input_fname, allow_maxshield=True).load_data()
    if bad_ch :
        raw.info['bads'].extend(bad_ch)
    duration=len(raw.times)/1000
    raw_tsss = mne.preprocessing.maxwell_filter(raw, calibration=sss_cal, cross_talk=ct_sparse, st_duration=duration, coord_frame='head', destination=mean_hp_fname, verbose=True)
    raw_tsss.save(output_fname, overwrite=True) 
    

import mne
from mne_bids import BIDSPath, write_raw_bids


raw = mne.io.read_raw_fif('rest1_anon.fif', allow_maxshield=True)

bp = BIDSPath(subject='01',
                  session="ses",
                  task="task",
                  root="REST",
                  datatype='meg',
                  suffix='meg',
                  extension='.fif')
raw.info['line_freq'] = 50
    
write_raw_bids(raw, bp, overwrite=True)
    
raw_tsss = mne.preprocessing.maxwell_filter(raw, st_duration=10)
bp = BIDSPath(subject='01',
                  session="ses",
                  task="task",
                  processing="mnemaxwell",
                  root="REST",
                  datatype='meg',
                  suffix='meg',
                  extension='.fif')
write_raw_bids(raw_tsss, bp, overwrite=True, allow_preload=True, format='FIF')

raw_mxf = mne.io.read_raw_fif('rest1_anon_tsss.fif')

bp = BIDSPath(subject='01',
                  session="ses",
                  task="task",
                  processing="maxfilter",
                  root="REST",
                  datatype='meg',
                  suffix='meg',
                  extension='.fif')
raw.info['line_freq'] = 50
    
write_raw_bids(raw_mxf, bp, overwrite=True)


raw_mxf = mne.io.read_raw_fif('rest1_anon_tsss.fif')

bp = BIDSPath(subject='01',
                  session="ses",
                  task="task",
                  processing="mxfnotransf",
                  root="REST",
                  datatype='meg',
                  suffix='meg',
                  extension='.fif')
raw.info['line_freq'] = 50
    
write_raw_bids(raw_mxf, bp, overwrite=True, allow_preload=True, format='FIF')
    
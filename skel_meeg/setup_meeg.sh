#!/bin/bash
#
script_path="`dirname $(realpath $0)`"

echo "$script_path"
ls -l "$script_path"

echo Copying desktop links
rsync -auxlv "$script_path"/Desktop/*.desktop ~/Desktop
rsync -auxlv "$script_path"/Bureau/*.desktop ~/Bureau

echo Completing .bashrc and .bash_profile
cat "$script_path/add_bashrc" >> ~/.bashrc
cat "$script_path/add_bash_profile" >> ~/.bash_profile

echo Done.

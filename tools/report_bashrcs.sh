#!/bin/bash
#
# Use command :
# /network/lustre/iss02/cenir/analyse/meeg/10_SCRIPTS/tools/report_bashrcs.sh
#

output_path="/network/lustre/iss02/cenir/analyse/meeg/10_SCRIPTS/tools/results"

scp -r ~/.bashrc "$output_path/"$USER"@"$(hostname)"_bashrc.txt"
scp -r ~/.bash_profile "$output_path/"$USER"@"$(hostname)"_bash_profile.txt"

setfacl -m u:meeg:rw "$output_path/"$USER"@"$(hostname)"_bashrc.txt"
setfacl -m u:meeg:rw "$output_path/"$USER"@"$(hostname)"_bash_profile.txt"


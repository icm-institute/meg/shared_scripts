#!/bin/bash
#
# Fichier à sourcer dans les scripts bash de traitement de données MEG-EEG
# Ajouter la ligne suivante dans les scripts
#   source /network/lustre/iss02/cenir/analyse/meeg/10_SCRIPTS/config_meeg.sh

# suppression des fichiers core
ulimit -c 0 

# Variables Globales
export ROOT_CENIR="/network/lustre/iss02/cenir"
export GLOBAL="${ROOT_CENIR}/software/meeg"
export DATA_DIRECTORY="/network/lustre/iss02/cenir/analyse/meeg"
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:.:/opt/bin:/usr/X11R6/bin:/usr/bin/X11"
export NEUROMAG_ROOT="${GLOBAL}/neuromag"

# General
export PATH=${GLOBAL}/bincenir:${PATH}:${GLOBAL}/bin

export PATH=${GLOBAL}/neuromag/bin/util:${GLOBAL}/neuromag/bin/X11:${PATH}
alias maxfilter_cenir="\maxfilter -ctc sss_config/ct_sparse.fif -cal sss_config/sss_cal.dat"

# lien vers fast_tf
alias fast_tf="${GLOBAL}/fast_tf5/bin/fast_tf"
alias fast_statcond="${GLOBAL}/fast_tf5/bin/fast_statcond"
alias fast_statcondpaired="${GLOBAL}/fast_tf5/bin/fast_statcondpaired"
alias mega_fast_tf="fast_tf"
alias mega_fast_statcond="fast_statcond"
alias mega_fast_statcondpaired="fast_statcondpaired"

# liens vers multiconv
alias multiconv="${GLOBAL}/multiconv/bin/multiconv"
alias multiconv_dev="${GLOBAL}/multiconv/bin/multiconv_dev"
alias multiconv_prec="${GLOBAL}/multiconv/bin/multiconv_prec"

# liens vers ptxtool
alias ptxtool="${GLOBAL}/ptxtool/bin/ptxtool"
alias ptxtool_dev="${GLOBAL}/ptxtool/bin/ptxtool_dev"
alias ptxtool_prec="${GLOBAL}/ptxtool/bin/ptxtool_prec"

